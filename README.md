# Dyndns_online.net

```
usage: dyndns.py [-h] [-v | -q] [-c] [-t TOKEN] [-u URL] [-r RECORDS]
                 domain

Update dns zone with online.net API

positional arguments:
  domain                domain to update

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose
  -q, --quiet
  -c, --clean           clean old unused zones
  -t TOKEN, --token TOKEN
                        token's API
                        (https://console.online.net/fr/api/access)
  -u URL, --url URL     url to get public ip, default=http://ifconfig.me
  -r RECORDS, --records RECORDS
                        records to update, comma separated list
```
